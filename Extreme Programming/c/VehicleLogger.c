#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "VehicleLogger.h"

static struct VehicleLogger *logger;

int is_enabled = 0;

void (*startEngine)(void);
void (*stopEngine)(void);
void (*setSpeed)(short speedInKmh);
void (*steer)(short angleInDegrees);
void (*setFlashers)(_Bool status);
unsigned int (*getAmountOfFuelInL)(void);

void startEngine_logger()
{
    if (is_enabled)
    {
        fprintf(logger->logFile, "Engine started\n");
    }
    startEngine();
}

void stopEngine_logger()
{
    if (is_enabled)
    {
        fprintf(logger->logFile, "Engine stopped\n");
    }
    stopEngine();
}

void setSpeed_logger(short speedInKmh)
{
    if (is_enabled)
    {
        fprintf(logger->logFile, "Speed set to %d km/h\n", speedInKmh);
    }
    setSpeed(speedInKmh);
}

void steer_logger(short angleInDegrees)
{
    if (is_enabled)
    {
        fprintf(logger->logFile, "Steered of %d°\n", angleInDegrees);
    }
    steer(angleInDegrees);
}

void setFlashers_logger(_Bool status)
{
    if (is_enabled)
    {
        if (status)
        {
            fprintf(logger->logFile, "Flashers turned on\n");
        }
        else
        {
            fprintf(logger->logFile, "Flashers turned off\n");
        }
    }
    setFlashers(status);
}
unsigned int getAmountOfFuelInL_logger()
{
    unsigned int amountOfFuelInL = getAmountOfFuelInL();
    if (is_enabled)
    {
        fprintf(logger->logFile, "Amount of fuel remaining in tank: %dL\n", amountOfFuelInL);
    }
    return amountOfFuelInL;
}

struct VehicleLogger *VehicleLogger_new(struct VehicleInterface *vehicleCommands,
                                        struct ReferencedObject *vehicleReferencedObject)
{
    struct ReferencedObject *newReferencedObject = ReferencedObject_new();
    FILE *newLogFile = fopen("log.txt", "a+");

    setbuf(newLogFile, NULL);
    fprintf(newLogFile, "\n\n------------ NEW EXECUTION ------------\n\n");

    struct VehicleLogger *pointer = malloc(sizeof(struct VehicleLogger));

    ReferencedObject_reference(vehicleReferencedObject);
    pointer->vehicleReferencedObject = vehicleReferencedObject;
    pointer->referencedObject = newReferencedObject;
    pointer->logFile = newLogFile;

    startEngine = vehicleCommands->startEngine;
    vehicleCommands->startEngine = startEngine_logger;

    stopEngine = vehicleCommands->stopEngine;
    vehicleCommands->stopEngine = stopEngine_logger;

    setSpeed = vehicleCommands->setSpeed;
    vehicleCommands->setSpeed = setSpeed_logger;

    steer = vehicleCommands->steer;
    vehicleCommands->steer = steer_logger;

    setFlashers = vehicleCommands->setFlashers;
    vehicleCommands->setFlashers = setFlashers_logger;

    logger = pointer;

    return pointer;
}

void VehicleLogger_delete(struct VehicleLogger *logger)
{
    stopLogging();

    if (ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(logger->referencedObject))
    {
        fprintf(logger->logFile, "\n\n------------ END OF EXECUTION ------------\n\n");
        fclose(logger->logFile);

        if (ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(logger->vehicleReferencedObject))
        {
            free(logger->vehicleReferencedObject);
        }

        free(logger);
    }
}

void startLogging(void)
{
    is_enabled = 1;
}

void stopLogging(void)
{
    is_enabled = 0;
}
