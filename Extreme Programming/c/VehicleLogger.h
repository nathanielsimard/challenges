#ifndef VEHICLELOGGER_H_
#define VEHICLELOGGER_H_

#include <stdio.h>
#include "ReferencedObject.h"
#include "VehicleInterface.h"

struct VehicleLogger
{
    struct ReferencedObject *referencedObject;
    struct ReferencedObject *vehicleReferencedObject;

    /* adjust the type of these attributes
    
    _A_TYPE_ originalVehicleCommands;
    _A_TYPE_ actualVehicleCommands;

    */
    FILE *logFile;
};

struct VehicleLogger *VehicleLogger_new(struct VehicleInterface *vehicleCommands, struct ReferencedObject *vehicleReferencedObject);
void VehicleLogger_delete(struct VehicleLogger *logger);
void startLogging(void);
void stopLogging(void);

#endif
