# OS

## Super quiz!

### Instruction
Vous devez répondre au questionnaire directement dans ce README en dessous de chaque question. Ces questions touchent plusieurs aspects de la programmation de bas niveau et les réponses sont généralement assez courtes.

###### Question 1
Quelle est la différence entre le heap et le stack (le monceau et la pile).

La heap sert à faire de l'allocation dynamique de la mémoire. Par exemple une struct créée sera encore valid même après le `scope` de la fonction qui l'a créée.
La stack sert à avoir de la mémoire static. Si on crée une instance d'une struct via la stack, elle ne sera plus valid après le `scope`.
La mémoire de la stack se libère automatiquement après que son `scope` soit terminé.
La mémoire de la heap doit être libéré manuellement via `free`.

###### Question 2
Que fais la fonction malloc?

Elle permet de réservé de la mémoire dynamique sur la heap et retourne un pointer qui pointe vers l'addresse mémoire.

###### Question 3
Décrivez le fonctionnement de ce code.

```
uint16_t lastKeys = 0;
uint16_t diff, currentKeys;

while (1) {
	currentKeys = scanKeyboard();
	if ((diff = (uint16_t) (currentKeys & (~lastKeys))) || !currentKeys)
		lastKeys = currentKeys;
	faire_quelque_chose(diff);
}
```

Dans une boucle infini, on optient les clés courantes d'un clavier. S'il y a un changement des clés courantes on met à jour les anciennes clés et on fait quelque chose 
avec la différence des clés.

###### Question 4
Que signifie les 8 paramètres de cette commande GCC:  
`gcc -Os -Wall -g3 -flto -lpthread -o main main.c`



###### Question 5
À quoi sert le DMA dans un microcontrolleur ou un ordinateur.


###### Question 6
Quelle est la différence entre mutex et sémaphore?


###### Question 7
Un microcontrolleur lit, avec son convertisseur analogique à numérique à 12 bits, la température d'une cuve de fermentation à l'aide d'un thermomètre. Votre microcontrolleur fonctionne en 5V. Le thermomètre retourne une valeur entre 0 et 5V correspondant aux températures respectivement entre 15-25 degrées Celcius. Assumez que le comportement du thermomètre est linéaire.
Donnez la précision de lecture de votre microcontrolleur. Dans quel type de variable devriez-vous sauvegarder la valeur lue de façon à conserver le maximum de précision (char, int, long, float, double, etc), pourquoi? S'il y a lieu, expliquez aussi à quoi correspondent les valeurs enregistrées. (Max 10 lignes)


###### Question 8
Expliquer une façon de faire le "debounce" d'un bouton.


###### Question 9
Comment pourriez-vous faire pour mesurer la capacitance (en Farrad) d'un condensateur à l'aide d'un Arduino ?


###### Question 10
Que fait ce code et à quoi peut-il servir ? Existe-t-il un équivalent dans la librairie standard du C?

`while (*p++ = *q++) ;`


###### Question 11
Décrivez les principales fonctions d'un microprocesseur.
