const game = require("./game/game");
var elements;
var score;
var isGameOver = false;
var actionQueue = [];

startNewGame();

var WALL = 0;
var EMPTY = 5;
var COIN = 6

const FACTOR = 1;
var table;
var player;
var direction;

function addWall(position, dimenssion) {
    var x = Math.floor(position.x / FACTOR);
    var y = Math.floor(position.y / FACTOR);
    table[x][y] = WALL;
    var dim_x = Math.floor(3 * dimenssion.x / FACTOR);
    var dim_y = Math.floor(3 * dimenssion.y / FACTOR);

    for (var i = x - dim_x; i < x + dim_x; i++) {
        for (var j = y - dim_y; j < y + dim_y; j++) {
            if (i > 0 && j > 0) {
                table[i][j] = WALL;
            }
        }
    }
}

function updateTable(elements) {
    table = [];
    direction = null;
    table.length = Math.floor(130 / FACTOR);

    for (var j = 0; j < Math.floor(130 / FACTOR); j++) {
        var row = [];
        row.length = Math.floor(130 / FACTOR);
        row.fill(EMPTY);
        table[j] = row;
    }

    elements.forEach(element => {
        if (element.name == "player") {
            player = element;
            table[Math.floor(player.position.x / FACTOR)][Math.floor(player.position.y / FACTOR)] = 10;
        } else if (element.name == "wall") {
            var position = element.position;
            if (position.x < 0 || position.y < 0) return;
            addWall(position, element.size);
        } else if (element.name == "coin") {
            var position = element.position;
            if (position.x < 0 || position.y < 0) return;
            if (!direction) {
                direction = element.position;
            } else if (Math.abs(direction.x - player.position.x) + Math.abs(direction.y - player.position.y) >
                Math.abs(position.x - player.x) + Math.abs(position.y - player.position.y)) {
                direction = element.position;
            }
            table[Math.floor(position.x / FACTOR)][Math.floor(position.y / FACTOR)] = COIN;
        }
    });
    if (!direction) {
        direction = {
            x: 30,
            y: 75,
        }
    }
}


function movePlayer() {
    var elements = game.getElements();
    updateTable(elements);
    console.log("Want to go to :");
    console.log(direction);
    var graph = new Graph(table);
    var playerPosition = player.position;
    var start = graph.grid[Math.floor(playerPosition.x / FACTOR)][Math.floor(playerPosition.y / FACTOR)];
    var end = graph.grid[Math.floor(direction.x / FACTOR)][Math.floor(direction.y / FACTOR)];
    var result = astar.search(graph, start, end);
    //  console.log(result);

    console.log(player.position);
    nextTile = result[1];
    if (!nextTile) {
        //        1onsole.log("Don't turn");
        return;
    }
    var nextX = nextTile.x;
    var nextY = nextTile.y;
    //    console.log("next X : " + nextX);
    //    console.log("current X : " + Math.floor(playerPosition.x));
    if (nextX * 100 > Math.floor(playerPosition.x * 100 / FACTOR)) {
        console.log("Turn right");
        goRight();
    } else if (nextX * 100 < Math.floor(playerPosition.x * 100 / FACTOR)) {
        console.log("Turn left");
        goLeft();
    } else {
        console.log("Don't turn");
    }
}

async function startNewGame() {
    await sleep(1000);
    game.startGame();
    await sleep(500);

    while (!isGameOver) {
        await sleep(100);
        if (actionQueue.length > 0) {
            actionQueue[0]();
            actionQueue.shift();
        }
        elements = game.getElements();
        score = game.getScore();
        isGameOver = game.isGameOver();
        movePlayer();
    }
}

function goRight() {
    actionQueue.push(game.goRight);
}

function goLeft() {
    actionQueue.push(game.goLeft);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
